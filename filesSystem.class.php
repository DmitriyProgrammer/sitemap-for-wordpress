<?php

class FilesSystem
{
    
    // функция возвращает массив файла production.txt
    public function arrayReadFile()
    {
        
        $array_production = file(ABSPATH.'wp-content/plugins/sitemap-its-easy/production/production.txt'); 
        
        for($i=0;$i<count($array_production);$i++)
        {
        
            $pat = "!@%@(.*)@%@!sUi";
            $n = preg_match_all($pat, $array_production[$i], $result);

            for ($m=0;$m<$n;$m++) {

                $pat2 = "!@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@!sUi";
                $n2 = preg_match_all($pat2, $result[1][$m], $result2);
                
                $array_file[$i]['ID'] = $result2[1][0];
                $array_file[$i]['post_modified_gmt'] = $result2[3][0];
                $array_file[$i]['url'] = $result2[2][0];
                
            }
        
        }
        
    return $array_file;
        
    }
    
    // функция возвращает массив файла production.txt с первым индексом - ID БД
    public function arrayByIDReadFile()
    {
        
        $array_production = file(ABSPATH.'wp-content/plugins/sitemap-its-easy/production/production.txt'); 
        
        for($i=0;$i<count($array_production);$i++)
        {
        
            $pat = "!@%@(.*)@%@!sUi";
            $n = preg_match_all($pat, $array_production[$i], $result);

            for ($m=0;$m<$n;$m++) {

                $pat2 = "!@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@!sUi";
                $n2 = preg_match_all($pat2, $result[1][$m], $result2);
                
                $array_file[$result2[1][0]]['post_modified_gmt'] = $result2[3][0];
                $array_file[$result2[1][0]]['url'] = $result2[2][0];
                $array_file[$result2[1][0]]['changefreq'] = $result2[4][0];
                $array_file[$result2[1][0]]['prioritet'] = $result2[5][0];
                
            }
        
        }
        
    return $array_file;
        
    }
    
    // запись файла production.txt
    public function saveFile($id='',$url = '',$lastmod = '',$changefreq = '',$priority = '')
    {
        
        $fp = fopen($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sitemap-its-easy/production/production.txt', "a"); // Открываем файл в режиме записи 
        $mytext = "@%@@&@".$id."@&@".$url."@&@".$lastmod."@&@".$changefreq."@&@".$priority."@&@@%@\r\n"; // Исходная строка
        $test = fwrite($fp, $mytext); // Запись в файл
        if ($test) {echo 'Данные в файл успешно занесены.';}
        else {echo 'Ошибка при записи в файл.';}
        fclose($fp); //Закрытие файла
        
    }
    
    // удаления из файла production.txt
    public function deleteFile($id='',$url = '',$lastmod = '',$changefreq = '',$priority = '')
    {

        $file=file($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sitemap-its-easy/production/production.txt');

        $fp=fopen($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sitemap-its-easy/production/production.txt',"w");

            for($i=0;$i<count($file);$i++)
            {

                $pat = "!@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@!sUi";
                $n = preg_match_all($pat, $file[$i], $result);
                // если полученый урл аргумента равен урлу строки в файле production.txt
                if($url == $result[2][0])
                {
                    unset($file[$i]);
                }

            }

        fputs($fp,implode("",$file));

        fclose($fp);
   
    }
    
    // удаления содержимого из файла production.txt
    public function deleteAllFromFile()
    {

        $fp=fopen($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sitemap-its-easy/production/production.txt',"w");

        $test = fwrite($fp, ''); // Запись в файл

        fclose($fp);
   
    }
    
    // изменение файла production.txt
    public function modifyFile($id='',$url = '',$lastmod = '',$changefreq = '',$priority = '')
    {

        $file=file($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sitemap-its-easy/production/production.txt');

        $fp=fopen($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sitemap-its-easy/production/production.txt',"w");

            for($i=0;$i<count($file);$i++)
            {

                $pat = "!@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@!sUi";
                $n = preg_match_all($pat, $file[$i], $result);
                // если полученый урл аргумента равен урлу строки в файле production.txt
                if($url == $result[2][0])
                {
                    
                    $mytext = "@%@@&@".$id."@&@".$url."@&@".$lastmod."@&@".$changefreq."@&@".$priority."@&@@%@\r\n";
                    $test = fwrite($fp, $mytext); // Запись в файл
                    
                }else
                {
                    
                    $mytext = $file[$i];
                    $test = fwrite($fp, $mytext); // Запись в файл
                    
                }

            }

        fclose($fp);
        
    }
    
    public function addFileSM()
    {

        $file=file($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sitemap-its-easy/production/production.txt');

        $fp=fopen($_SERVER['DOCUMENT_ROOT'].'/sitemap.xml',"w");

        $begin = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
';
        $test = fwrite($fp, $begin); // Запись в файл
        
            for($i=0;$i<count($file);$i++)
            {

                $pat = "!@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@!sUi";
                $n = preg_match_all($pat, $file[$i], $result);
                    
                $mytext = "<url>
<loc>".$result[2][0]."</loc>
<lastmod>".$result[3][0]."</lastmod>
<changefreq>".$result[4][0]."</changefreq>
<priority>".$result[5][0]."</priority>
</url>
";
                $test = fwrite($fp, $mytext); // Запись в файл
                    
            }

            $test = fwrite($fp, '</urlset>'); // Запись в файл
            
        fclose($fp);
        
    }
    

    
    public function downloadFileSM($fileWay = '',$fileName = '')
    {
        // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
        // если этого не сделать файл будет читаться в память полностью!
        if (ob_get_level()) {
          ob_end_clean();
        }
            // заставляем браузер показать окно сохранения файла
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($fileName));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            // читаем файл и отправляем его пользователю
            if ($fd = fopen($fileWay, 'rb')) {
              while (!feof($fd)) {
                print fread($fd, 1024);
              }
              fclose($fd);
            }
            exit;
        
        }
}