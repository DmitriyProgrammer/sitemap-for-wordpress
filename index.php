<?php

/**
 * @package Sitemap-its-easy
 * @version 1.0
 */
/*
Plugin Name: Sitemap-it-is-easy
Plugin URI: http://ithazard.com/
Description: This plugin, I made for you my friend. Create, modify file sitemap. Good luck!
Author: Dmitriy Georgievich Shchekalov
Version: 1.0
Author URI: http://ithazard.com/
*/

//require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/language/en_us.lang.php');
require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/config.php');
require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/getContent.class.php');
require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/clientPreferences.class.php');
require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/compareSitemapBD.class.php');

add_action('admin_menu', 'addNamePlaginToMenu');

function addNamePlaginToMenu() {
    
	add_options_page('Plagin - "Sitemap-it-is-easy"', 'Sitemap-it-is-easy', 'manage_options', NAMEPLUGIN.'.php', 'sitemapPage');

}

function sitemapPage(){
    
    $object_cp = new ClientPreferences();
    $object_cp->visualization();
   
}