<?php

class ClientPreferences
{

    // сборка визуализации
    function visualization()
    {

        $step = filter_input(INPUT_GET, 'step');
        
        switch ($step) {
        case 'first':
            $this->showComapareSmOnBd();
            break;
        case 'second':
            $this->readFile();
            break;
        case 'third':
            $this->addFileSMapLocalDir();
            $this->generatorSM();
            break;
        default:
            $this->blankPage();
        }

//        $this->showComapareBdOnSm(); 

    }

    // стартовая страница - Начало
    private function blankPage()
    {
        
        require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/language/en_us.lang.php');
        require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/language/ru_RU.lang.php');
        
        echo('<p align="center"><font size="5">'.$lang_strings[LANGUAGE]['LBL_MAIN_PAGE_TITLE'].'</font></p>');
        
        echo('<form action="#">
            <br>
                    <input type="hidden" name="page" value="'.NAMEPLUGIN.'">
                    <input type="hidden" name="step" value="first">
                    <input type="submit" value="'.$lang_strings[LANGUAGE]['LBL_BUTTON_STEP_1'].' >>>">
              </form>');
        
        echo('<p align="left"><font size="5">'.$lang_strings[LANGUAGE]['LBL_TITLE_STEP_1'].' - '.$lang_strings[LANGUAGE]['LBL_STEP_1_DESCRIPTION'].'<br>
        '.$lang_strings[LANGUAGE]['LBL_TITLE_STEP_2'].' - '.$lang_strings[LANGUAGE]['LBL_STEP_2_DESCRIPTION'].'<br>
        '.$lang_strings[LANGUAGE]['LBL_TITLE_STEP_3'].' - '.$lang_strings[LANGUAGE]['LBL_STEP_3_DESCRIPTION'].'</font></p>
        ');
        
        echo('<form action="/wp-content/plugins/sitemap-its-easy/handlerCleanError.php" style="display:none">
                    <input type="submit" value="Удаление ошибок">
              </form>');
        
        echo('<p align="left"><font size="5" style="display:none">Кнопка "Удаление ошибок", нужна для возврата плагина в первоначальное состояние,
с очисткой всех временных файлов.</font></p>');
        
    }
    
    // функция выводит на экран посты, которых нет в файле sitemap - Шаг №1
    private function showComapareSmOnBd()
    {
    
    require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/language/en_us.lang.php');
    require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/language/ru_RU.lang.php');
        
    echo('<p align="left"><font size="5">'.$lang_strings[LANGUAGE]['LBL_TITLE_STEP_1'].'</font></p>');
        
    echo('<form action="#" style="display: inline-block;float: right;">
        <br>
                <input type="hidden" name="page" value="'.NAMEPLUGIN.'">
                <input type="hidden" name="step" value="second">
                <input type="submit" value="'.$lang_strings[LANGUAGE]['LBL_TITLE_STEP_2'].' >>>">
          </form>');
    
    echo('<form action="#" style="display: inline-block;">
        <br>
                <input type="hidden" name="page" value="'.NAMEPLUGIN.'">
                <input type="submit" value="Вернуться в начало">
          </form>');
    
    echo('
        <style>
            table {
            border-spacing: 0 10px;
            font-family: \'Open Sans\', sans-serif;
            font-weight: bold;
            }
            th {
            background: #56433D;
            color: #F9C941;
            border-right: 2px solid; 
            width: 10%;
            }
            td {
            vertical-align: middle;
            text-align: center;
            border-top: 2px solid #56433D;
            border-bottom: 2px solid #56433D;
            border-right: 2px solid #56433D;
            width: 10%;
            }
        </style>
    ');
        
        $object_compareSitemapBD = new compareSitemapBD();
        $result_compareSmOnBd = $object_compareSitemapBD->compareSmOnBd();
        
        if($result_compareSmOnBd)
        {
        
            echo('<p align="center"><font size="5">'.$lang_strings[LANGUAGE]['LBL_TABLE_1_TITLE'].'</font></p>');

            $object_content = new GetContent();
            $listview = $object_content->getAllPageAndPostByID();

            $i=0;
            foreach ($result_compareSmOnBd as $key) 
            {

                echo('
                    <form action="/wp-content/plugins/sitemap-its-easy/handlerFormsSave.php">
                    <table class="table">
                    ');
                if($i==0)
                {
                echo('
                        <thead>
                            <tr>
                                <th style="display:none"><input type="checkbox"></th>
                                <th style="display:none">ID поста</th>
                                <th>URL</th>
                                <th>Дата последеней модификации</th>
                                <th>Частота обновлений</th>
                                <th>Приоритет</th>
                                <th></th>
                            </tr>
                        </thead>
                    ');
                }
                echo('<tbody>
                            <tr>
                                <td style="display:none"><input type="checkbox" name="checkbox_'.$i.'"><input type="hidden" name="i" value="'.$i.'"></td>
                                <td style="display:none"><input type="text" value="'.$key.'" name="id"></td>
                                <td><textarea name="url" readonly>'.urldecode($listview[$key]['url']).'</textarea></td>
                                <td><input type="text" value="'.urldecode($listview[$key]['post_modified_gmt']).'" name="datelm"></td>
                                <td>
                                    <select name="changefreq">
                                        <option>always</option>
                                        <option>hourly</option>
                                        <option>daily</option>
                                        <option selected>weekly</option>
                                        <option>monthly</option>
                                        <option>yearly</option>
                                        <option>never</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="priority">
                                        <option>0.0</option>
                                        <option>0.1</option>
                                        <option>0.2</option>
                                        <option>0.3</option>
                                        <option>0.4</option>
                                        <option selected>0.5</option>
                                        <option>0.6</option>
                                        <option>0.7</option>
                                        <option>0.8</option>
                                        <option>0.9</option>
                                        <option>1.0</option>
                                    </select>
                                </td>
                                <td><input type="submit" value="'.$lang_strings[LANGUAGE]['LBL_TABLE_1_BUTTON'].'"></td>
                            </tr>
                        </tbody>
                    </table>
                    </form>
                ');
            $i++;
            }


            
//            echo('</form>');
            
        }else
        {
            echo('<p align="left"><font size="5">Все посты сайта "'.get_site_url().'", добавлены в sitemap.xml, можно переходить ко второму шагу.</font></p>');
        }

    }

    // функци выводит на экран, данные из файла sitemap.xml(точнее из промежуточной версии production.txt) - Шаг №2
    private function readFile()
    {

    require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/language/en_us.lang.php');
    require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/language/ru_RU.lang.php');
        
        echo('<p align="left"><font size="5">'.$lang_strings[LANGUAGE]['LBL_TITLE_STEP_1'].'</font></p>');
        
        echo('<form action="#" style="display: inline-block;">
            <br>
                    <input type="hidden" name="page" value="'.NAMEPLUGIN.'">
                    <input type="hidden" name="step" value="first">
                    <input type="submit" value="<<< '.$lang_strings[LANGUAGE]['LBL_TITLE_STEP_1'].'">
              </form>');
        
        echo('<form action="#" style="display: inline-block;float: right;">
            <br>
                    <input type="hidden" name="page" value="'.NAMEPLUGIN.'">
                    <input type="hidden" name="step" value="third">
                    <input type="submit" value="'.$lang_strings[LANGUAGE]['LBL_TITLE_STEP_3'].' >>>">
              </form>');
        
        $array_production = file(ABSPATH.'wp-content/plugins/sitemap-its-easy/production/production.txt'); 
        
        echo('
            <style>
                table {
                border-spacing: 0 10px;
                font-family: \'Open Sans\', sans-serif;
                font-weight: bold;
                }
                th {
                background: #56433D;
                color: #F9C941;
                border-right: 2px solid; 
                width: 10%;
                }
                td {
                vertical-align: middle;
                text-align: center;
                border-top: 2px solid #56433D;
                border-bottom: 2px solid #56433D;
                border-right: 2px solid #56433D;
                width: 10%;
                }
            </style>
        ');
        
        if($array_production)
        {
            echo('<p align="center"><font size="5">'.$lang_strings[LANGUAGE]['LBL_TABLE_2_TITLE'].'</font></p>');
        }  else {
            echo('<p align="center"><font size="5">Ни один пост не добавлен в sitemap.xml, вернитесь на шаг №1 и добавьте посты в файл sitemap.xml</font></p>');
        }
            
        for($i=0;$i<count($array_production);$i++)
        {
        
        echo('
            <form action="/wp-content/plugins/sitemap-its-easy/handlerFormReadFile.php">
            <table class="table">
        ');
        
        if($i==0)
        {
        echo('
                <thead>
                    <tr>
                        <th style="display:none"><input type="checkbox"></th>
                        <th style="display:none">ID поста</th>
                        <th>URL</th>
                        <th>Дата последеней модификации</th>
                        <th>Частота обновлений</th>
                        <th>Приоритет</th>
                        <th>Изменить запись</th>
                        <th>Удалить запись</th>
                    </tr>
                </thead>
            ');
        }
        
        echo('
                <tbody>
            ');
            
            $pat = "!@%@(.*)@%@!sUi";
            $n = preg_match_all($pat, $array_production[$i], $result);

            for ($m=0;$m<$n;$m++) {

                $pat2 = "!@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@!sUi";
                $n2 = preg_match_all($pat2, $result[1][$m], $result2);

                $always='';$hourly='';$daily='';$weekly='';$monthly='';$yearly='';$never='';
                
                    switch ($result2[4][0]) {
                    case 'always':
                        $always = 'selected';
                        break;
                    case 'hourly':
                        $hourly = 'selected';
                        break;
                    case 'daily':
                        $daily = 'selected';
                        break;
                    case 'weekly':
                        $weekly = 'selected';
                        break;
                    case 'monthly':
                        $monthly = 'selected';
                        break;
                    case 'yearly':
                        $yearly = 'selected';
                        break;
                    case 'never':
                        $never = 'selected';
                        break;
                    default:
                        
                    }
                    
                $o0 = '';$o1 = '';$o2 = '';$o3 = '';$o4 = '';$o5 = '';$o6 = '';$o7 = '';$o8 = '';$o9 = '';$o10 = '';

                    switch ($result2[5][0]) {
                    case '0.0':
                        $o0 = 'selected';
                        break;
                    case '0.1':
                        $o1 = 'selected';
                        break;
                    case '0.2':
                        $o2 = 'selected';
                        break;
                    case '0.3':
                        $o3 = 'selected';
                        break;
                    case '0.4':
                        $o4 = 'selected';
                        break;
                    case '0.5':
                        $o5 = 'selected';
                        break;
                    case '0.6':
                        $o6 = 'selected';
                        break;
                    case '0.7':
                        $o7 = 'selected';
                        break;
                    case '0.8':
                        $o8 = 'selected';
                        break;
                    case '0.9':
                        $o9 = 'selected';
                        break;
                    case '1.0':
                        $o10 = 'selected';
                        break;
                    default:
                        
                    }
                    
                    echo('<tr>
                                <td style="display:none"><input type="checkbox" name="checkbox_'.$i.'"><input type="hidden" name="i" value="'.$i.'"></td>
                                <td style="display:none"><input type="text" value="'.$result2[1][0].'" name="id"></td>
                                <td><textarea name="url" readonly>'.$result2[2][0].'</textarea></td>
                                <td><input type="text" value="'.$result2[3][0].'" name="datelm" onblur="undisable(\''.$i.'\');"></td>
                                <td>
                                    <select name="changefreq" onchange="undisable(\''.$i.'\');">
                                        <option '.$always.'>always</option>
                                        <option '.$hourly.'>hourly</option>
                                        <option '.$daily.'>daily</option>
                                        <option '.$weekly.'>weekly</option>
                                        <option '.$monthly.'>monthly</option>
                                        <option '.$yearly.'>yearly</option>
                                        <option '.$never.'>never</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="priority" onchange="undisable(\''.$i.'\');">
                                        <option '.$o0.'>0.0</option>
                                        <option '.$o1.'>0.1</option>
                                        <option '.$o2.'>0.2</option>
                                        <option '.$o3.'>0.3</option>
                                        <option '.$o4.'>0.4</option>
                                        <option '.$o5.'>0.5</option>
                                        <option '.$o6.'>0.6</option>
                                        <option '.$o7.'>0.7</option>
                                        <option '.$o8.'>0.8</option>
                                        <option '.$o9.'>0.9</option>
                                        <option '.$o10.'>1.0</option>
                                    </select>
                                </td>
                                <td><input type="submit" value="'.$lang_strings[LANGUAGE]['LBL_TABLE_2_BUTTON1'].'" name="modifySM" disabled="disabled" id="modifySM_'.$i.'"></td>
                                <td><input type="submit" value="'.$lang_strings[LANGUAGE]['LBL_TABLE_2_BUTTON2'].'" name="delSM"></td>
                          </tr>
                        ');

            }
        
        echo('
                </tbody>
            </table>
            </form>
            ');
            
        }
        
        echo("<script>
                function undisable(vars)
                {
                    var b = document.querySelector('#modifySM_'+vars);
                    b.removeAttribute('disabled', 'disabled');
                }
        </script>");
        
    }
    
    // Шаг №3
    private function generatorSM()
    {
        
    require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/language/en_us.lang.php');
    require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/language/ru_RU.lang.php');
        
        echo('<p align="left"><font size="5">'.$lang_strings[LANGUAGE]['LBL_TITLE_STEP_3'].'</font></p>');
        
        echo('
            <style>
                table {
                border-spacing: 0 10px;
                font-family: \'Open Sans\', sans-serif;
                font-weight: bold;
                }
                th {
                background: #56433D;
                color: #F9C941;
                border-right: 2px solid; 
                width: 10%;
                }
                td {
                vertical-align: middle;
                text-align: center;
                border-top: 2px solid #56433D;
                border-bottom: 2px solid #56433D;
                border-right: 2px solid #56433D;
                width: 10%;
                }
            </style>
        ');

        echo('<form action="#" style="display: inline-block">
            <br>
                    <input type="hidden" name="page" value="'.NAMEPLUGIN.'">
                    <input type="hidden" name="step" value="second">
                    <input type="submit" value="<<< '.$lang_strings[LANGUAGE]['LBL_TITLE_STEP_2'].'">
              </form>');
        
        echo('<form action="#" style="display: inline-block;float: right;">
            <br>
                    <input type="hidden" name="page" value="'.NAMEPLUGIN.'">
                    <input type="submit" value="Вернуться в начало">
              </form>');
        
        echo('<p align="center"><font size="5">'.$lang_strings[LANGUAGE]['LBL_TABLE_3_TITLE'].'</font></p>');
       
        echo('
            <form action="/wp-content/plugins/sitemap-its-easy/handlerGeneratorSM.php">
                <input type="hidden" name="fileWay" value="'.get_site_url().'/wp-content/plugins/sitemap-its-easy/production/sitemap.xml">
                <input type="hidden" name="fileName" value="sitemap.xml">
            <table class="table">
        ');
        
        echo('
                <thead>
                    <tr>
                        <th>Добавить Sitemap.xml в корень сайта '.get_site_url().'</th>
                        <th>Просмотр Sitemap.xml</th>
                        <th>Скачать Sitemap.xml</th>
                    </tr>
                </thead>
            ');
        
        echo('
                <tbody>
            ');
        
                    echo('<tr>
                                <td><input type="submit" value="'.$lang_strings[LANGUAGE]['LBL_TABLE_3_BUTTON1'].'" name="addsitemap"></td>
                                <td><a href="'.get_site_url().'/wp-content/plugins/sitemap-its-easy/production/sitemap.xml" target="_blank">'.$lang_strings[LANGUAGE]['LBL_TABLE_3_BUTTON2'].'</a></td>
                                <td><input type="submit" value="'.$lang_strings[LANGUAGE]['LBL_TABLE_3_BUTTON3'].'" name="downloadsitemap"></td>
                          </tr>
                        ');
                    
        echo('
                </tbody>
            </table>
            </form>
            ');

    }
    
    // Функция выводит на экран - посты sitemap.xml, которых нет в БД
    private function showComapareBdOnSm()
    {

    echo('
        <style>
            table {
            border-spacing: 0 10px;
            font-family: \'Open Sans\', sans-serif;
            font-weight: bold;
            }
            th {
            background: #56433D;
            color: #F9C941;
            border-right: 2px solid; 
            width: 10%;
            }
            td {
            vertical-align: middle;
            text-align: center;
            border-top: 2px solid #56433D;
            border-bottom: 2px solid #56433D;
            border-right: 2px solid #56433D;
            width: 10%;
            }
        </style>
    ');
        
        $object_compareSitemapBD = new compareSitemapBD();
        $result_compareBdOnSm = $object_compareSitemapBD->compareBdOnSm();
        
        if($result_compareBdOnSm)
        {
        
            echo('<p align="center"><font size="5">В файл sitemap.xml посты, которых нет в БД:</font></p>');

            $object_filesSystem = new FilesSystem();
            $listview = $object_filesSystem->arrayByIDReadFile();

            $i=0;
            foreach ($result_compareBdOnSm as $key) {

                echo('
                    <form action="/wp-content/plugins/sitemap-its-easy/handlerFormsDelete.php">
                    <table class="table">
                    ');
                if($i==0)
                {
                echo('
                        <thead>
                            <tr>
                                <th style="display:none"></th>
                                <th style="display:none">ID поста</th>
                                <th>URL</th>
                                <th>Дата последеней модификации</th>
                                <th>Частота обновлений</th>
                                <th>Приоритет</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                    ');
                }
                
                $always='';$hourly='';$daily='';$weekly='';$monthly='';$yearly='';$never='';
                
                    switch ($listview[$key]['changefreq']) {
                    case 'always':
                        $always = 'selected';
                        break;
                    case 'hourly':
                        $hourly = 'selected';
                        break;
                    case 'daily':
                        $daily = 'selected';
                        break;
                    case 'weekly':
                        $weekly = 'selected';
                        break;
                    case 'monthly':
                        $monthly = 'selected';
                        break;
                    case 'yearly':
                        $yearly = 'selected';
                        break;
                    case 'never':
                        $never = 'selected';
                        break;
                    default:
                        
                    }
                    
                $o0 = '';$o1 = '';$o2 = '';$o3 = '';$o4 = '';$o5 = '';$o6 = '';$o7 = '';$o8 = '';$o9 = '';$o10 = '';
                
                    switch ($listview[$key]['prioritet']) {
                    case '0.0':
                        $o0 = 'selected';
                        break;
                    case '0.1':
                        $o1 = 'selected';
                        break;
                    case '0.2':
                        $o2 = 'selected';
                        break;
                    case '0.3':
                        $o3 = 'selected';
                        break;
                    case '0.4':
                        $o4 = 'selected';
                        break;
                    case '0.5':
                        $o5 = 'selected';
                        break;
                    case '0.6':
                        $o6 = 'selected';
                        break;
                    case '0.7':
                        $o7 = 'selected';
                        break;
                    case '0.8':
                        $o8 = 'selected';
                        break;
                    case '0.9':
                        $o9 = 'selected';
                        break;
                    case '1.0':
                        $o10 = 'selected';
                        break;
                    default:
                        
                    }
                
                        echo('<tr>
                                    <td style="display:none"><input type="checkbox" name="checkbox_'.$i.'"><input type="hidden" name="i" value="'.$i.'"></td>
                                    <td style="display:none"><input type="text" value="'.$key.'" name="id"></td>
                                    <td><textarea name="url" readonly>'.$listview[$key]['url'].'</textarea></td>
                                    <td><input type="text" value="'.$listview[$key]['post_modified_gmt'].'" name="datelm"></td>
                                    <td>
                                    <select name="changefreq">
                                        <option '.$always.'>always</option>
                                        <option '.$hourly.'>hourly</option>
                                        <option '.$daily.'>daily</option>
                                        <option '.$weekly.'>weekly</option>
                                        <option '.$monthly.'>monthly</option>
                                        <option '.$yearly.'>yearly</option>
                                        <option '.$never.'>never</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="priority">
                                        <option '.$o0.'>0.0</option>
                                        <option '.$o1.'>0.1</option>
                                        <option '.$o2.'>0.2</option>
                                        <option '.$o3.'>0.3</option>
                                        <option '.$o4.'>0.4</option>
                                        <option '.$o5.'>0.5</option>
                                        <option '.$o6.'>0.6</option>
                                        <option '.$o7.'>0.7</option>
                                        <option '.$o8.'>0.8</option>
                                        <option '.$o9.'>0.9</option>
                                        <option '.$o10.'>1.0</option>
                                    </select>
                                    </td>
                                <td><input type="submit" value="Удалить из Sitemap"></td>
                              </tr>
                            </tbody>
                            </table>
                            ');
            $i++;
            }
        }
    }
    
    private function addFileSMapLocalDir()
    {

        $file=file($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sitemap-its-easy/production/production.txt');

        $fp=fopen($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sitemap-its-easy/production/sitemap.xml',"w");

        $begin = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
';
        $test = fwrite($fp, $begin); // Запись в файл
        
            for($i=0;$i<count($file);$i++)
            {

                $pat = "!@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@(.*)@&@!sUi";
                $n = preg_match_all($pat, $file[$i], $result);
                    
                $mytext = "<url>
<loc>".$result[2][0]."</loc>
<lastmod>".$result[3][0]."</lastmod>
<changefreq>".$result[4][0]."</changefreq>
<priority>".$result[5][0]."</priority>
</url>
";
                $test = fwrite($fp, $mytext); // Запись в файл
                    
            }

            $test = fwrite($fp, '</urlset>'); // Запись в файл
            
        fclose($fp);
        
    }
    
}