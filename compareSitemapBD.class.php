<?php

require_once(ABSPATH.'wp-content/plugins/sitemap-its-easy/filesSystem.class.php');

class compareSitemapBD
{
    
    // функция сравнивает файл sitemap с БД, возвращает разницу
    function compareSmOnBd()
    {
    
        $fs = new FilesSystem();
        
        $array_production = $fs->arrayReadFile();
        
//        echo('<pre>');
//        print_r($array_production);
//        echo('</pre>');
        
        $object_lv = new GetContent();

        $pageAndPost = $object_lv->getArrayPageAndPost();
        
//        echo('<pre>');
//        print_r($pageAndPost);
//        echo('</pre>');

        
        
        for($i=0;$i<count($pageAndPost);$i++)
        {

            $temp = 'not found';
            
            for($t=0;$t<count($array_production);$t++)
            {
                
                if($pageAndPost[$i]['ID'] == $array_production[$t]['ID'])
                {
                    
                    $temp = 'found';
                    
                }
                
            }
            
            if($temp == 'not found')
            {

                $arrai_not_found_id[] = $pageAndPost[$i]['ID'];
                
            }
            
            

        }
        
        return $arrai_not_found_id;
        
    }
    
    // функция сравнивает БД с файлом production.txt, разницу возвращает
    function compareBdOnSm()
    {
    
        $fs = new FilesSystem();
        
        $array_production = $fs->arrayReadFile();

        $object_lv = new GetContent();

        $pageAndPost = $object_lv->getArrayPageAndPost();
        
        for($i=0;$i<count($array_production);$i++)
        {

            $temp = 'not found';
            
            for($t=0;$t<count($pageAndPost);$t++)
            {
//                echo($array_production[$i]['url'].'-'.$pageAndPost[$t]['url'].'<br>');
                if($array_production[$i]['url'] == $pageAndPost[$t]['url'])
                {
                    
                    $temp = 'found';
                    
                }
                
            }
            
            if($temp == 'not found')
            {

                $arrai_not_found_id[] = $array_production[$i]['ID'];
                
            }

        }
        
        return $arrai_not_found_id;
        
    }
    
}