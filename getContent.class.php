<?php

class GetContent {

    // функция возвращает массив страниц из БД
    private function getAllPage ()
    {
    
        $params_page = array(
                'post_type' => 'page',
                'orderby' => 'date',
                'order' => 'DESC',
                'posts_per_page' => 50000,
                'suppress_filters' => true
        );
        
        $page_list = array();
        $postslist_page = array();
        
        $postslist_page = get_posts($params_page);

        $i=0;
        foreach( $postslist_page as $page ){
            $page_list[$i]['ID'] = $postslist_page[$i]->ID;
            // $page_list[$i]['post_modified_gmt'] = urldecode($postslist_page[$i]->post_modified_gmt);
            $page_list[$i]['post_modified_gmt'] = strstr(urldecode($postslist_page[$i]->post_modified_gmt), ' ', true);
            $page_list[$i]['url'] = get_site_url().'/'.urldecode($postslist_page[$i]->post_name.'/');
            $page_list[$i]['post_name'] = urldecode($postslist_page[$i]->post_name);
            $i++;
        }
        
        return $page_list;
    
    }

    // функция возвращает массив постов из БД
    private function getAllPost()
    {
    
        $params_post = array(
                'post_type' => 'post',
                'orderby' => 'date',
                'order' => 'DESC',
                'posts_per_page' => 50000,
                'suppress_filters' => true
        );
        
        $post_list = array();
        $postslist_post = array();
        
        $postslist_post = get_posts($params_post);

        $i=0;
        foreach( $postslist_post as $post ){
            $post_list[$i]['ID'] = urldecode($postslist_post[$i]->ID);
            // $post_list[$i]['post_modified_gmt'] = urldecode($postslist_post[$i]->post_modified_gmt);
            $post_list[$i]['post_modified_gmt'] = strstr(urldecode($postslist_post[$i]->post_modified_gmt), ' ', true);
            $post_list[$i]['url'] = get_site_url().'/'.urldecode($postslist_post[$i]->post_name.'/');
            $post_list[$i]['post_name'] = urldecode($postslist_post[$i]->post_name);
            $i++;
        }
        
        return $post_list;
    
    }
    
    // функция возвращает упордоченый массив постов и страниц из БД
    public function getArrayPageAndPost()
    {
        
        $page = $this->getAllPage();
        
        $post = $this->getAllPost();
        

        
        $listview = array_merge($page,$post); 
        
        return $listview;
        
    }
    
    // функция возвращает массив страниц по ID из БД
    private function getAllPageByID ()
    {
    
        $params_page = array(
                'post_type' => 'page',
                'orderby' => 'date',
                'order' => 'DESC',
                'posts_per_page' => 50000,
                'suppress_filters' => true
        );
        
        $page_list = array();
        $postslist_page = array();
        
        $postslist_page = get_posts($params_page);

        $i=0;
        foreach( $postslist_page as $page ){
            // $page_list[$postslist_page[$i]->ID]['post_modified_gmt'] = urldecode($postslist_page[$i]->post_modified_gmt);
            $page_list[$postslist_page[$i]->ID]['post_modified_gmt'] = strstr(urldecode($postslist_page[$i]->post_modified_gmt), ' ', true);
            $page_list[$postslist_page[$i]->ID]['url'] = get_site_url().'/'.urldecode($postslist_page[$i]->post_name.'/');
            $page_list[$postslist_page[$i]->ID]['post_name'] = urldecode($postslist_page[$i]->post_name);
            $i++;
        }
        
        return $page_list;
    
    }

    // функция возвращает массив постов по ID из БД
    private function getAllPostByID()
    {
    
        $params_post = array(
                'post_type' => 'post',
                'orderby' => 'date',
                'order' => 'DESC',
                'posts_per_page' => 50000,
                'suppress_filters' => true
        );
        
        $post_list = array();
        $postslist_post = array();
        
        $postslist_post = get_posts($params_post);

        $i=0;
        foreach( $postslist_post as $post ){
            // $post_list[$postslist_post[$i]->ID]['post_modified_gmt'] = urldecode($postslist_post[$i]->post_modified_gmt);
            $post_list[$postslist_post[$i]->ID]['post_modified_gmt'] = strstr(urldecode($postslist_post[$i]->post_modified_gmt), ' ', true);
            $post_list[$postslist_post[$i]->ID]['url'] = get_site_url().'/'.urldecode($postslist_post[$i]->post_name.'/');
            $post_list[$postslist_post[$i]->ID]['post_name'] = urldecode($postslist_post[$i]->post_name);
            $i++;
        }
        
        return $post_list;
    
    }
    
    // функция возвращает упордоченый массив постов и страниц из БД по ID
    public function getAllPageAndPostByID()
    {
        
        $page = $this->getAllPageByID();
        
        $post = $this->getAllPostByID();
        
        $listview = $page + $post;
        
        return $listview;
        
    }
    
}